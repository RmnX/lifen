package fr.lifen.util;

import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;

import static org.junit.Assert.*;

/**
 * Test for class fr.lifen.util.DateUtil.
 */
public class DateUtilTest {

    @Test
    public void testIsItSaturdayOrSunday_True_Sunday() throws Exception {

        LocalDate dateRepresentation = LocalDate.of(2017,1,1);
        assertEquals(dateRepresentation.getDayOfWeek(), DayOfWeek.SUNDAY);
        assertTrue(DateUtil.isItSaturdayOrSunday(dateRepresentation));
    }

    @Test
    public void testIsItSaturdayOrSunday_True_Saturday() throws Exception {
        LocalDate dateRepresentation = LocalDate.of(2017, 1, 7);
        assertEquals(dateRepresentation.getDayOfWeek(), DayOfWeek.SATURDAY);
        assertTrue(DateUtil.isItSaturdayOrSunday(dateRepresentation));
    }

    @Test
    public void testIsItSaturdayOrSunday_False_Monday() throws Exception {
        LocalDate dateRepresentation = LocalDate.of(2017, 1, 2);
        assertEquals(dateRepresentation.getDayOfWeek(), DayOfWeek.MONDAY);
        assertFalse(DateUtil.isItSaturdayOrSunday(dateRepresentation));
    }

    @Test
    public void testIsItSaturdayOrSunday_False_Tuesday() throws Exception {
        LocalDate dateRepresentation = LocalDate.of(2017, 1, 3);
        assertEquals(dateRepresentation.getDayOfWeek(), DayOfWeek.TUESDAY);
        assertFalse(DateUtil.isItSaturdayOrSunday(dateRepresentation));
    }

    @Test
    public void testIsItSaturdayOrSunday_False_Wednesday() throws Exception {
        LocalDate dateRepresentation = LocalDate.of(2017, 1, 4);
        assertEquals(dateRepresentation.getDayOfWeek(), DayOfWeek.WEDNESDAY);
        assertFalse(DateUtil.isItSaturdayOrSunday(dateRepresentation));
    }

    @Test
    public void testIsItSaturdayOrSunday_False_Thursday() throws Exception {
        LocalDate dateRepresentation = LocalDate.of(2017, 1, 5);
        assertEquals(dateRepresentation.getDayOfWeek(), DayOfWeek.THURSDAY);
        assertFalse(DateUtil.isItSaturdayOrSunday(dateRepresentation));
    }

    @Test
    public void testIsItSaturdayOrSunday_False_Friday() throws Exception {
        LocalDate dateRepresentation = LocalDate.of(2017, 1, 6);
        assertEquals(dateRepresentation.getDayOfWeek(), DayOfWeek.FRIDAY);
        assertFalse(DateUtil.isItSaturdayOrSunday(dateRepresentation));
    }


}