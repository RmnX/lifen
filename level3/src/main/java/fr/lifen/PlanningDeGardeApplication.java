package fr.lifen;

import fr.lifen.entity.in.Planning;
import fr.lifen.entity.in.Worker;
import fr.lifen.entity.out.Result;
import fr.lifen.entity.out.WorkerCost;
import fr.lifen.io.Reader;
import fr.lifen.io.Writer;
import fr.lifen.util.DateUtil;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class PlanningDeGardeApplication {

    /**
     *
     * @param args arguments optionnels du programme :
     *             1er : Chemin du fichier contennant les données d'entrée
     *             2eme : Chemin du fichier de sortie contenant la somme à payer à chaque travailleur
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final PlanningDeGardeApplication app = new PlanningDeGardeApplication();
        Planning planning = Reader.launchAndRead(args.length > 0 ? Optional.of(args[0]) : Optional.empty());
        Result result = app.execute(planning);
        Writer.write(args.length > 1 ? Optional.of(args[1]) : Optional.empty(), result);
    }

    private Result execute(Planning planning) throws IOException {

        final BigDecimal multiplicatorEndOfTheWeek = new BigDecimal("2");
        // Process data
        final Map<Long, BigDecimal> workers = planning.getWorkers().stream().collect(Collectors.toMap(Worker::getId, worker -> worker.getStatus().getPrice()));

        // For each entry of map we have worker Id as key and its cost as value
        Map<Long, BigDecimal> workerIdsCost = new HashMap<>();

        planning.getShifts().stream()
                // Take only shift with userId
                .filter(s -> s.getUserId() != null)
                        //
                .forEach(shift -> {
                    final Long workerId = shift.getUserId();
                    BigDecimal shiftCost = DateUtil.isItSaturdayOrSunday(shift.getStartDate()) ?
                            workers.get(workerId).multiply(multiplicatorEndOfTheWeek) : workers.get(workerId);
                    if (workerIdsCost.containsKey(workerId)) {
                        shiftCost = shiftCost.add(workerIdsCost.get(workerId));
                    }
                    workerIdsCost.put(workerId, shiftCost);
                });

        // Convert map into List of workerCost
        final List<WorkerCost> workerCosts = workerIdsCost.entrySet().stream()
                .map(entry -> new WorkerCost(entry.getKey(), entry.getValue())).collect(Collectors.toList());

        return new Result(workerCosts);

    }


}
