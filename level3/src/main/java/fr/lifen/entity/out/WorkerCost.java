package fr.lifen.entity.out;

import java.math.BigDecimal;

public class WorkerCost {

    private Long id;

    private BigDecimal price;

    public WorkerCost() {
    }

    public WorkerCost(Long id, BigDecimal price) {
        this.id = id;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
