package fr.lifen.entity.in;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import fr.lifen.json.LocalDateDeserializer;

import java.time.LocalDate;

public class Shift {

    private Long id;

    @JsonProperty("planning_id")
    private Long planningId;

    @JsonProperty("user_id")
    private Long userId;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonProperty("start_date")
    private LocalDate startDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlanningId() {
        return planningId;
    }

    public void setPlanningId(Long planningId) {
        this.planningId = planningId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }
}
