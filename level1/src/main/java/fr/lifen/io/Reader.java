package fr.lifen.io;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.lifen.entity.in.Planning;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public final class Reader {
    
    public static Planning launchAndRead(Optional<String> pathInputFile) throws IOException {
        Planning planning;
        final String dataPath = "/data.json";
        final String expMsg = "Le chemin du fichier de data est incorrect";

        // JSON Mapper
        final ObjectMapper mapper = new ObjectMapper();


        // Use internal resources
        if (!pathInputFile.isPresent()) {
            try (final InputStream inputStream = Reader.class.getResourceAsStream(dataPath)) {
                planning = mapper.readValue(inputStream, Planning.class);
            }
        }
        //Use specific file
        else{
            File inputFile = new File(pathInputFile.get());
            if(!inputFile.exists()){
                throw new IllegalArgumentException(expMsg);
            }
            try (final InputStream inputStream = new FileInputStream(inputFile)) {
                planning = mapper.readValue(inputStream, Planning.class);
            }
        }
        return planning;
    }
}
