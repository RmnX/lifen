package fr.lifen.entity.in;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class Worker {

    private Long id;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("price_per_shift")
    private BigDecimal pricePerShift;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public BigDecimal getPricePerShift() {
        return pricePerShift;
    }

    public void setPricePerShift(BigDecimal pricePerShift) {
        this.pricePerShift = pricePerShift;
    }
}
