package fr.lifen.entity.out;

import java.util.List;

public class Result {

    private List<WorkerCost> workers;

    public Result() {}

    public Result(List<WorkerCost> workers) {
        this.workers = workers;
    }

    public List<WorkerCost> getWorkers() {
        return workers;
    }

}
