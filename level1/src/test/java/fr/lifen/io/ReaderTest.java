package fr.lifen.io;

import fr.lifen.entity.in.Planning;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;

/**
 * Test for class fr.lifen.service.Reader.
 */
public class ReaderTest {

    @Test
    public void testLaunchAndRead_without_args() throws Exception {

        //Prepare

        //Act
        Planning planning = Reader.launchAndRead(Optional.empty());

        //Verify
        assertThat(planning.getWorkers()).hasSize(4)
                .extracting("id", "firstName", "pricePerShift")
                .contains(tuple(1L, "Julie", new BigDecimal(230)),
                        tuple(2L, "Marc", new BigDecimal(100)),
                        tuple(3L, "Antoine", new BigDecimal(230)),
                        tuple(4L, "Emilie", new BigDecimal(100)));

        assertThat(planning.getShifts()).hasSize(10)
                .extracting("id", "userId", "startDate")
                .contains(tuple(1L, 1L, LocalDate.of(2017, 1, 1)),
                        tuple(2L, 2L, LocalDate.of(2017, 1, 2)),
                        tuple(3L, 3L, LocalDate.of(2017, 1, 3)),
                        tuple(4L, 4L, LocalDate.of(2017, 1, 4)),
                        tuple(5L, 1L, LocalDate.of(2017, 1, 5)),
                        tuple(6L, 2L, LocalDate.of(2017, 1, 6)),
                        tuple(7L, 3L, LocalDate.of(2017, 1, 7)),
                        tuple(8L, 4L, LocalDate.of(2017, 1, 8)),
                        tuple(9L, 1L, LocalDate.of(2017, 1, 9)),
                        tuple(10L, 2L,LocalDate.of(2017, 1, 10)));

    }

    @Test
    public void testLaunchAndRead_with_args() throws Exception {

        //Prepare

        //Act
        Planning planning = Reader.launchAndRead(Optional.of("src/test/resources/dataTest.json"));

        //Verify
        assertThat(planning.getWorkers()).hasSize(2)
                .extracting("id", "firstName", "pricePerShift")
                .contains(tuple(1L, "Julie", new BigDecimal(230)),
                        tuple(2L, "Marc", new BigDecimal(100)));

        assertThat(planning.getShifts()).hasSize(4)
                .extracting("id", "userId", "startDate")
                .contains(tuple(1L, 1L, LocalDate.of(2017, 1, 1)),
                        tuple(2L, 2L, LocalDate.of(2017, 1, 3)),
                        tuple(3L, null, LocalDate.of(2017, 1, 4)),
                        tuple(4L, 1L, LocalDate.of(2017, 1, 5)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLaunchAndRead_with_invalid_args() throws Exception {
        Reader.launchAndRead(Optional.of("invalid.json"));
    }


}