# Run level 1

Utilisation
===========

Version with the integrated files in the archive, the result will be accessible on file output.json:

    $mvn clean package
    $java -jar target/planning-de-garde-1.0.one-jar.jar
or

Version with the files to specify, the first is the data file and the second the result file :

    $mvn clean package
    $java -jar planning-de-garde-1.0.one-jar.jar "Path/Of/Data/File" "Path/Of/Result/File"
