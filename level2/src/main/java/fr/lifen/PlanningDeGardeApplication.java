package fr.lifen;

import fr.lifen.entity.in.Planning;
import fr.lifen.entity.in.Shift;
import fr.lifen.entity.in.Worker;
import fr.lifen.entity.out.Result;
import fr.lifen.entity.out.WorkerCost;
import fr.lifen.io.Reader;
import fr.lifen.io.Writer;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class PlanningDeGardeApplication {

    /**
     *
     * @param args arguments optionnels du programme :
     *             1er : Chemin du fichier contennant les données d'entrée
     *             2eme : Chemin du fichier de sortie contenant la somme à payer à chaque travailleur
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final PlanningDeGardeApplication app = new PlanningDeGardeApplication();
        Planning planning = Reader.launchAndRead(args.length > 0 ? Optional.of(args[0]) : Optional.empty());
        Result result = app.execute(planning);
        Writer.write(args.length > 1 ? Optional.of(args[1]) : Optional.empty(), result);
    }

    private Result execute(Planning planning) throws IOException {

        // Process data
        final Map<Long, BigDecimal> workers = planning.getWorkers().stream().collect(Collectors.toMap(Worker::getId, worker -> worker.getStatus().getPrice()));

        final List<WorkerCost> workerCosts =
                planning.getShifts().stream()
                        // Take only shift with userId
                        .filter(shift -> shift.getUserId() != null)
                        // Build map grouped by worker_id and sum the number of shifts as value
                        .collect(Collectors.groupingBy(Shift::getUserId, Collectors.counting()))
                        // Multiply shifts by pricePerShift
                        .entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, entry -> workers.get(entry.getKey()).multiply(new BigDecimal(entry.getValue()))))
                        // Build list of workerCost
                        .entrySet().stream().map(entry -> new WorkerCost(entry.getKey(), entry.getValue())).collect(Collectors.toList());

        return new Result(workerCosts);

    }


}
