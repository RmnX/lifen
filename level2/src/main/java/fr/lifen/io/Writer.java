package fr.lifen.io;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import fr.lifen.entity.out.Result;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Optional;

public final class Writer {
    
    public static void write(Optional<String> outputPathfile, Result result) throws IOException {

        // JSON Mapper
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

        final String outPath = "output.json";
        final File outputFile;

        if (!outputPathfile.isPresent()) {
            outputFile = new File(outPath);
            outputFile.createNewFile();
        }
        else{
            outputFile = new File(outputPathfile.get());
            if(outputFile.getParentFile() != null) {
                outputFile.getParentFile().mkdirs();
            }
            outputFile.createNewFile();
        }

        try (final OutputStream outputStream = new FileOutputStream(outputFile)) {
            mapper.writeValue(outputStream, result);
        }

    }
}
