package fr.lifen.entity.in;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Worker {

    private Long id;

    @JsonProperty("first_name")
    private String firstName;

    private Status status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
