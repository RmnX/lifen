package fr.lifen.entity.in;

import java.util.List;

public class Planning {

    private List<Worker> workers;
    private List<Shift> shifts;

    public List<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(List<Worker> workers) {
        this.workers = workers;
    }

    public List<Shift> getShifts() {
        return shifts;
    }

    public void setShifts(List<Shift> shifts) {
        this.shifts = shifts;
    }
}
