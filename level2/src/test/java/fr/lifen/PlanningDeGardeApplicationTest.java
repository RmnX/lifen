package fr.lifen;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.lifen.entity.out.Result;
import org.junit.AfterClass;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

/**
 * Test for class fr.lifen.PlanningDeGardeApplication.
 */
public class PlanningDeGardeApplicationTest {

    private final static String inputPathfileSpecific = "src/test/resources/dataTest.json";
    private final static String outputPathfileSpecific = "out/out.json";
    private final static String outputPathfileDefault = "output.json";

    @AfterClass
    public static void clean(){
        List<String> pathfileToDelete = Arrays.asList(outputPathfileSpecific, outputPathfileDefault);
        pathfileToDelete.stream().forEach(pathfile -> {
            File file = new File(pathfile);
            if(file.exists()){
                File parentfile = file.getParentFile();
                file.delete();
                if(file.getParentFile()!=null){
                    parentfile.delete();
                }
            }
        });

    }

    @Test
    public void testMain_Ok_WithoutArgs() throws Exception {
        String[] args = new String[]{};
        PlanningDeGardeApplication.main(args);
        File file = new File(outputPathfileDefault);
        final ObjectMapper mapper = new ObjectMapper();
        try (FileInputStream fis = new FileInputStream(file)) {
            final Result result = mapper.readValue(fis, Result.class);
            assertThat(result.getWorkers()).hasSize(4)
                    .extracting("id", "price")
                    .contains(tuple(1L, new BigDecimal(810)),
                            tuple(2L, new BigDecimal(810)),
                            tuple(3L, new BigDecimal(252)),
                            tuple(4L, new BigDecimal(540)));

        }

    }

    @Test
    public void testMain_Ok_WithArgs() throws Exception {
        String[] args = new String[]{inputPathfileSpecific, outputPathfileSpecific};
        PlanningDeGardeApplication.main(args);
        File file = new File(outputPathfileSpecific);
        final ObjectMapper mapper = new ObjectMapper();
        try (FileInputStream fis = new FileInputStream(file)) {
            final Result result = mapper.readValue(fis, Result.class);
            assertThat(result.getWorkers()).hasSize(3)
                    .extracting("id", "price")
                    .contains(tuple(1L, new BigDecimal(1080)),
                            tuple(7L, new BigDecimal(378)),
                            tuple(4L, new BigDecimal(540)));

        }

    }

    @Test(expected = IllegalArgumentException.class)
    public void testMain_Ko_WithInvalidArgs() throws Exception {
        String[] args = new String[]{"invalid.json", "out.json"};
        PlanningDeGardeApplication.main(args);
    }


}