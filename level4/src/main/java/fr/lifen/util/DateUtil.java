package fr.lifen.util;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class DateUtil {

    public static boolean isItSaturdayOrSunday(LocalDate date){
        return date.getDayOfWeek().equals(DayOfWeek.SATURDAY) || date.getDayOfWeek().equals(DayOfWeek.SUNDAY);
    }

}
