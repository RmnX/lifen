package fr.lifen.entity.in;

import com.fasterxml.jackson.annotation.JsonValue;

import java.math.BigDecimal;

public enum Status {
    MEDIC("medic", new BigDecimal(270)),
    INTERNE("interne", new BigDecimal(126)),
    INTERIM("interim", new BigDecimal(480));

    private String name;
    private BigDecimal price;

    Status(String name, BigDecimal price){
        this.price = price;
        this.name = name;
    }

    @JsonValue
    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
