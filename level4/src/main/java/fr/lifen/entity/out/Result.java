package fr.lifen.entity.out;

import java.util.List;

public class Result {

    private List<WorkerCost> workers;

    private Commission commission;

    public Result() {}

    public Result(List<WorkerCost> workers, Commission commission) {
        this.workers = workers;
        this.commission = commission;
    }

    public List<WorkerCost> getWorkers() {
        return workers;
    }

    public Commission getCommission() {
        return commission;
    }

    public void setCommission(Commission commission) {
        this.commission = commission;
    }
}
