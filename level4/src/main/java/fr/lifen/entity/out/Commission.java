package fr.lifen.entity.out;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class Commission {

    @JsonProperty("pdg_fee")
    private BigDecimal pdgFee;

    @JsonProperty("interim_shifts")
    private long interimShift;

    public BigDecimal getPdgFee() {
        return pdgFee;
    }

    public void setPdgFee(BigDecimal pdgFee) {
        this.pdgFee = pdgFee;
    }

    public long getInterimShift() {
        return interimShift;
    }

    public void setInterimShift(long interimShift) {
        this.interimShift = interimShift;
    }
}
