package fr.lifen.io;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.lifen.entity.out.Commission;
import fr.lifen.entity.out.Result;
import fr.lifen.entity.out.WorkerCost;
import org.junit.AfterClass;
import org.junit.Test;

import java.io.File;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.*;

/**
 * Test for class fr.lifen.io.Writer.
 */
public class WriterTest {

    private final static String outputPathfileSpecific = "out/result.json";
    private final static String outputPathfileDefault = "output.json";


    @AfterClass
    public static void clean(){
        List<String> pathfileToDelete = Arrays.asList(outputPathfileSpecific, outputPathfileDefault);
        pathfileToDelete.stream().forEach(pathfile -> {
            File file = new File(pathfile);
            if(file.exists()){
                File parentfile = file.getParentFile();
                file.delete();
                if(file.getParentFile()!=null){
                    parentfile.delete();
                }
            }
        });

    }

    @Test
    public void testWrite_without_outputPathfile_with_null_result() throws Exception {
        //Prepare

        //Act
        Writer.write(Optional.empty(), null);

        //Verify
        File file = new File(outputPathfileDefault);
        assertTrue(file.exists());
        ObjectMapper mapper = new ObjectMapper();
        Object object = mapper.readValue(file, Object.class);
        assertNull(object);

    }

    @Test
    public void testWrite_without_outputPathfile_with_result() throws Exception {
        //Prepare
        WorkerCost workerCostA = new WorkerCost(1L, new BigDecimal("2"));
        WorkerCost workerCostB = new WorkerCost(10L, new BigDecimal("10"));
        List<WorkerCost> workers = Arrays.asList(workerCostA, workerCostB);
        Commission commission = new Commission();
        commission.setInterimShift(2);
        commission.setPdgFee(BigDecimal.ONE);
        Result resultIn = new Result(workers, commission);

        //Act
        Writer.write(Optional.empty(), resultIn);

        //Verify
        File file = new File(outputPathfileDefault);
        assertTrue(file.exists());
        ObjectMapper mapper = new ObjectMapper();
        Result resultOut = mapper.readValue(file, Result.class);
        assertThat(resultOut.getWorkers()).hasSize(2)
                .extracting("id", "price")
                .contains(tuple(1L, new BigDecimal(2)),
                        tuple(10L, new BigDecimal(10)));
        assertEquals(2, resultOut.getCommission().getInterimShift());
        assertEquals(BigDecimal.ONE, resultOut.getCommission().getPdgFee());


    }

    @Test
    public void testWrite_with_args() throws Exception {

        //Prepare
        WorkerCost workerCostA = new WorkerCost(100L, new BigDecimal("2"));
        WorkerCost workerCostB = new WorkerCost(10L, new BigDecimal("100"));
        List<WorkerCost> workers = Arrays.asList(workerCostA, workerCostB);
        Commission commission = new Commission();
        commission.setInterimShift(10);
        commission.setPdgFee(BigDecimal.ONE);
        Result resultIn = new Result(workers, commission);


        //Act
        Writer.write(Optional.of(outputPathfileSpecific), resultIn);

        //Verify
        File file = new File(outputPathfileSpecific);
        assertTrue(file.exists());
        ObjectMapper mapper = new ObjectMapper();
        Result resultOut = mapper.readValue(file, Result.class);
        assertThat(resultOut.getWorkers()).hasSize(2)
                .extracting("id", "price")
                .contains(tuple(100L, new BigDecimal(2)),
                        tuple(10L, new BigDecimal(100)));
        assertEquals(10, resultOut.getCommission().getInterimShift());
        assertEquals(BigDecimal.ONE, resultOut.getCommission().getPdgFee());

    }
}