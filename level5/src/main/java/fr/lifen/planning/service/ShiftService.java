package fr.lifen.planning.service;

import fr.lifen.planning.domain.Commission;
import fr.lifen.planning.domain.Result;
import fr.lifen.planning.domain.Shift;
import fr.lifen.planning.domain.Worker;
import fr.lifen.planning.domain.WorkerCost;
import fr.lifen.planning.domain.enumeration.WorkerStatus;
import fr.lifen.planning.repository.ShiftRepository;
import fr.lifen.planning.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Shift.
 */
@Service
@Transactional
public class ShiftService {

    private final Logger log = LoggerFactory.getLogger(ShiftService.class);

    private final ShiftRepository shiftRepository;

    public ShiftService(ShiftRepository shiftRepository) {
        this.shiftRepository = shiftRepository;
    }

    /**
     * Save a shift.
     *
     * @param shift the entity to save
     * @return the persisted entity
     */
    public Shift save(Shift shift) {
        log.debug("Request to save Shift : {}", shift);        return shiftRepository.save(shift);
    }

    /**
     * Get all the shifts.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Shift> findAll() {
        log.debug("Request to get all Shifts");
        List<Shift> shifts = shiftRepository.findAll();
        shifts.stream().forEach(s -> s.setCost(DateUtil.isItSaturdayOrSunday(s.getStartDate()) ? s.getWorker().getStatus().getPrice().multiply(new BigDecimal("2")) : s.getWorker().getStatus().getPrice()));
        return shifts;
    }


    /**
     * Get one shift by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Shift> findOne(Long id) {
        log.debug("Request to get Shift : {}", id);
        Optional<Shift> shift = shiftRepository.findById(id);
        shift.ifPresent(s -> s.setCost(DateUtil.isItSaturdayOrSunday(s.getStartDate()) ? s.getWorker().getStatus().getPrice().multiply(new BigDecimal("2")) : s.getWorker().getStatus().getPrice()));
        return shift;
    }

    /**
     * Delete the shift by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Shift : {}", id);
        shiftRepository.deleteById(id);
    }

    /**
     * Get all the costs.
     *
     * @return The general cost for the sum of each shift
     */
    @Transactional(readOnly = true)
    public Result getGeneralCost() {
        log.debug("Request to get all Workers");

        List<Shift> shifts = shiftRepository.findAll();
        List<Worker> workers = shifts.stream().map(elt -> elt.getWorker()).collect(Collectors.toList());

        // Process data
        final Map<Long, BigDecimal> workersMap = workers.stream().distinct()
            .collect(Collectors.toMap(Worker::getId, worker -> worker.getStatus().getPrice()));

        // For each entry of map we have worker Id as key and its cost as value
        Map<Worker, BigDecimal> workerIdsCost = new HashMap<>();


        shifts.stream()
            .forEach(shift -> {
                final Long workerId = shift.getWorker().getId();
                BigDecimal shiftCost = DateUtil.isItSaturdayOrSunday(shift.getStartDate()) ?
                    workersMap.get(workerId).multiply(new BigDecimal(2)) : workersMap.get(workerId);
                if (workerIdsCost.containsKey(shift.getWorker())) {
                    shiftCost = shiftCost.add(workerIdsCost.get(shift.getWorker()));
                }
                workerIdsCost.put(shift.getWorker(), shiftCost);
            });

        // Convert map into List of workerCost
        final List<WorkerCost> workerCosts = workerIdsCost.entrySet().stream()
            .map(entry -> new WorkerCost(entry.getKey(), entry.getValue())).collect(Collectors.toList());

        // Calculate commission
        final Commission commission = calculateCommission(workers, shifts, workerCosts);

        return new Result(workerCosts, commission);
    }

    private Commission calculateCommission(List<Worker> workers, List<Shift> shifts, List<WorkerCost> workerCosts){

        final BigDecimal feeInterim = new BigDecimal(80);
        final BigDecimal commissionPercent = new BigDecimal(0.05);

        List<Long> interimWorkersId = workers.stream()
            .filter(worker -> WorkerStatus.INTERIM.equals(worker.getStatus())).map(Worker::getId).collect(Collectors.toList());
        long interimShift = shifts.stream()
            .filter(s -> s.getWorker().getId() != null).filter(shift -> interimWorkersId.contains(shift.getWorker().getId())).count();


        BigDecimal sumWorkerCosts = workerCosts.stream().map(WorkerCost::getPrice).reduce(BigDecimal::add).get();

        Commission commission = new Commission();
        commission.setInterimShift(interimShift);
        commission.setPdgFee(sumWorkerCosts.multiply(commissionPercent).add(feeInterim.multiply(new BigDecimal(interimShift))).setScale(2, RoundingMode.HALF_UP));

        return commission;
    }
}
