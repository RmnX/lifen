package fr.lifen.planning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Shift.
 */
@Entity
@Table(name = "shift")
public class Shift implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("")
    private Worker worker;

    private transient BigDecimal cost;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Shift startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public Worker getWorker() {
        return worker;
    }

    public Shift worker(Worker worker) {
        this.worker = worker;
        return this;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Shift shift = (Shift) o;
        if (shift.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), shift.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    /*@Override
    public String toString() {
        return "Shift{" +
            "id=" + getId() +
            ", startDate='" + getStartDate() + "'" +
            ", cost='" + getCost() != null ? getCost().toString() : "" + "'" +
            "}";
    }*/

    @Override
    public String toString() {
        return "Shift{" +
            "id=" + id +
            ", startDate=" + startDate +
            ", worker=" + worker +
            ", cost=" + cost +
            '}';
    }
}
