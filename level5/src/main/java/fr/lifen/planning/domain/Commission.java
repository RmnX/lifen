package fr.lifen.planning.domain;

import java.math.BigDecimal;

/**
 * Created by RmnX on 30/07/2018.
 */
public class Commission {

    private BigDecimal pdgFee;

    private long interimShift;

    public BigDecimal getPdgFee() {
        return pdgFee;
    }

    public void setPdgFee(BigDecimal pdgFee) {
        this.pdgFee = pdgFee;
    }

    public long getInterimShift() {
        return interimShift;
    }

    public void setInterimShift(long interimShift) {
        this.interimShift = interimShift;
    }
}
