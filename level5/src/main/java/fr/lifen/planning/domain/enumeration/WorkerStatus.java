package fr.lifen.planning.domain.enumeration;

import java.math.BigDecimal;

/**
 * The WorkerStatus enumeration.
 */
public enum WorkerStatus {

    MEDIC("medic", new BigDecimal(270)),
    INTERNE("interne", new BigDecimal(126)),
    INTERIM("interim", new BigDecimal(480));

    private String name;
    private BigDecimal price;

    WorkerStatus(String name, BigDecimal price){
        this.price = price;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

}
