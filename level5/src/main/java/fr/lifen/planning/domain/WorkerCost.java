package fr.lifen.planning.domain;

import java.math.BigDecimal;

/**
 * Created by RmnX on 28/07/2018.
 */
public class WorkerCost {

    private Worker worker;

    private BigDecimal price;

    public WorkerCost() {
    }

    public WorkerCost(Worker worker, BigDecimal price) {
        this.worker = worker;
        this.price = price;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorkerId(Worker id) {
        this.worker = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
