package fr.lifen.planning.domain;

import java.util.List;

/**
 * Created by RmnX on 28/07/2018.
 */
public class Result {

    private List<WorkerCost> workers;

    private Commission commission;

    public Result() {}

    public Result(List<WorkerCost> workers, Commission commission) {
        this.workers = workers;
        this.commission = commission;
    }

    public List<WorkerCost> getWorkers() {
        return workers;
    }

    public Commission getCommission() {
        return commission;
    }

}
