package fr.lifen.planning.util;

import java.time.DayOfWeek;
import java.time.LocalDate;

/**
 * Created by RmnX on 29/07/2018.
 */
public class DateUtil {

    public static boolean isItSaturdayOrSunday(LocalDate date){
        return date.getDayOfWeek().equals(DayOfWeek.SATURDAY) || date.getDayOfWeek().equals(DayOfWeek.SUNDAY);
    }
}
