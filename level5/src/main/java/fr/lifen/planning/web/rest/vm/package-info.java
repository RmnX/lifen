/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.lifen.planning.web.rest.vm;
