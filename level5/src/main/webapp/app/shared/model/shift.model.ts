import { Moment } from 'moment';
import { IWorker } from 'app/shared/model//worker.model';

export interface IShift {
    id?: number;
    startDate?: Moment;
    worker?: IWorker;
    cost?: number;
}

export class Shift implements IShift {
    constructor(public id?: number, public startDate?: Moment, public worker?: IWorker, public cost?: number) {}
}
