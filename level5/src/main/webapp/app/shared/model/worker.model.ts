export const enum WorkerStatus {
    MEDIC = 'MEDIC',
    INTERIM = 'INTERIM',
    INTERNE = 'INTERNE'
}

export interface IWorker {
    id?: number;
    firstName?: string;
    status?: WorkerStatus;
}

export class Worker implements IWorker {
    constructor(public id?: number, public firstName?: string, public status?: WorkerStatus) {}
}
