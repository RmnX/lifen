import { Moment } from 'moment';

export interface IResult {
    workers?: any[];
    commission?: any;
}

export class Result implements IResult {
    constructor(public workers?: any[], public commission?: any) {}
}
