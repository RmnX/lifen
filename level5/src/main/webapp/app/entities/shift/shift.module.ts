import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PlanningSharedModule } from 'app/shared';
import {
    ShiftResultComponent,
    ShiftComponent,
    ShiftDetailComponent,
    ShiftUpdateComponent,
    ShiftDeletePopupComponent,
    ShiftDeleteDialogComponent,
    shiftRoute,
    shiftPopupRoute
} from './';

const ENTITY_STATES = [...shiftRoute, ...shiftPopupRoute];

@NgModule({
    imports: [PlanningSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [ShiftComponent, ShiftResultComponent, ShiftDetailComponent, ShiftUpdateComponent, ShiftDeleteDialogComponent, ShiftDeletePopupComponent],
    entryComponents: [ShiftComponent, ShiftUpdateComponent, ShiftDeleteDialogComponent, ShiftDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlanningShiftModule {}
