import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IShift } from 'app/shared/model/shift.model';
import { IResult } from 'app/shared/model/result.model';

type EntityResponseType = HttpResponse<IShift>;
type EntityResultResponseType = HttpResponse<IResult>;
type EntityArrayResponseType = HttpResponse<IShift[]>;

@Injectable({ providedIn: 'root' })
export class ShiftService {
    private resourceUrl = SERVER_API_URL + 'api/shifts';

    constructor(private http: HttpClient) {}

    create(shift: IShift): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(shift);
        return this.http
            .post<IShift>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(shift: IShift): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(shift);
        return this.http
            .put<IShift>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IShift>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IShift[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    result(): Observable<EntityResultResponseType> {
        return this.http
            .get<IResult>(`${this.resourceUrl}/cost`, { observe: 'response' })
            .pipe(map((res: EntityResultResponseType) => this.toto(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(shift: IShift): IShift {
        const copy: IShift = Object.assign({}, shift, {
            startDate: shift.startDate != null && shift.startDate.isValid() ? shift.startDate.format(DATE_FORMAT) : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.startDate = res.body.startDate != null ? moment(res.body.startDate) : null;
        return res;
    }

    private toto(res: EntityResultResponseType): EntityResultResponseType {
        console.log( res);
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((shift: IShift) => {
            shift.startDate = shift.startDate != null ? moment(shift.startDate) : null;
        });
        return res;
    }
}
