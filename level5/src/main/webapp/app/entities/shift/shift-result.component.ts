import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IResult } from 'app/shared/model/result.model';
import { Principal } from 'app/core';
import { ShiftService } from './shift.service';

@Component({
    selector: 'jhi-shift',
    templateUrl: './shift-result.compoment.html',
    styleUrls: ['../../../content/css/shift.css']
})
export class ShiftResultComponent implements OnInit, OnDestroy {
    result: IResult;
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private shiftService: ShiftService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadResult() {
        this.shiftService.result().subscribe(
            (res: HttpResponse<IResult>) => {
                this.result = res.body;
                console.log(this.result);
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadResult();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInShifts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInShifts() {
        this.eventSubscriber = this.eventManager.subscribe('shiftListModification', response => this.loadResult());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
