import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Worker } from 'app/shared/model/worker.model';
import { WorkerService } from './worker.service';
import { WorkerComponent } from './worker.component';
import { WorkerDetailComponent } from './worker-detail.component';
import { WorkerUpdateComponent } from './worker-update.component';
import { WorkerDeletePopupComponent } from './worker-delete-dialog.component';
import { IWorker } from 'app/shared/model/worker.model';

@Injectable({ providedIn: 'root' })
export class WorkerResolve implements Resolve<IWorker> {
    constructor(private service: WorkerService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((worker: HttpResponse<Worker>) => worker.body));
        }
        return of(new Worker());
    }
}

export const workerRoute: Routes = [
    {
        path: 'worker',
        component: WorkerComponent,
        data: {
            authorities: [],
            pageTitle: 'planningApp.worker.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'worker/:id/view',
        component: WorkerDetailComponent,
        resolve: {
            worker: WorkerResolve
        },
        data: {
            authorities: [],
            pageTitle: 'planningApp.worker.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'worker/new',
        component: WorkerUpdateComponent,
        resolve: {
            worker: WorkerResolve
        },
        data: {
            authorities: [],
            pageTitle: 'planningApp.worker.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'worker/:id/edit',
        component: WorkerUpdateComponent,
        resolve: {
            worker: WorkerResolve
        },
        data: {
            authorities: [],
            pageTitle: 'planningApp.worker.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const workerPopupRoute: Routes = [
    {
        path: 'worker/:id/delete',
        component: WorkerDeletePopupComponent,
        resolve: {
            worker: WorkerResolve
        },
        data: {
            authorities: [],
            pageTitle: 'planningApp.worker.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
