import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IWorker } from 'app/shared/model/worker.model';
import { Principal } from 'app/core';
import { WorkerService } from './worker.service';

@Component({
    selector: 'jhi-worker',
    templateUrl: './worker.component.html'
})
export class WorkerComponent implements OnInit, OnDestroy {
    workers: IWorker[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private workerService: WorkerService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.workerService.query().subscribe(
            (res: HttpResponse<IWorker[]>) => {
                this.workers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInWorkers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IWorker) {
        return item.id;
    }

    registerChangeInWorkers() {
        this.eventSubscriber = this.eventManager.subscribe('workerListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
