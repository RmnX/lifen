import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IWorker } from 'app/shared/model/worker.model';
import { WorkerService } from './worker.service';

@Component({
    selector: 'jhi-worker-update',
    templateUrl: './worker-update.component.html'
})
export class WorkerUpdateComponent implements OnInit {
    private _worker: IWorker;
    isSaving: boolean;

    constructor(private workerService: WorkerService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ worker }) => {
            this.worker = worker;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.worker.id !== undefined) {
            this.subscribeToSaveResponse(this.workerService.update(this.worker));
        } else {
            this.subscribeToSaveResponse(this.workerService.create(this.worker));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IWorker>>) {
        result.subscribe((res: HttpResponse<IWorker>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get worker() {
        return this._worker;
    }

    set worker(worker: IWorker) {
        this._worker = worker;
    }
}
