import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PlanningWorkerModule } from './worker/worker.module';
import { PlanningShiftModule } from './shift/shift.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        PlanningWorkerModule,
        PlanningShiftModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PlanningEntityModule {}
